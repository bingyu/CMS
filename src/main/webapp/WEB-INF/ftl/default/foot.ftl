<footer class="footer">
         <div class="container">
             <div class="row">
                 <div class="col-lg-3 col-sm-3">
                     <h1>联系信息</h1>
                     <address>
                         <p>地址:湖南省长沙市人民路159号</p>
                         <p>电话 : (123) 456-7890</p>
                         <p>传真 : (123) 456-7890</p>
                         <p>邮箱 : <a href="javascript:;">support@vectorlab.com</a></p>
                     </address>
                 </div>
                 <div class="col-lg-5 col-sm-5">
                     <h1>最近的微博动态</h1>
                     <div class="tweet-box">
                         <i class="icon-twitter"></i>
                         <em>请关注  <a href="javascript:;">@nettus</a> 我们最新的动态! <a href="javascript:;">weixin.com</a></em>
                     </div>
                 </div>
                 <div class="col-lg-3 col-sm-3 col-lg-offset-1">
                     <h1>其他方式联系</h1>
                     <ul class="social-link-footer list-unstyled">
                         <li><a href="#"><i class="icon-weibo"></i></a></li>
                         <li><a href="#"><i class="icon-renren"></i></a></li>
                     </ul>
                 </div>
             </div>
         </div>
     </footer>
  <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="${basePath}/default/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${basePath}/default/js/hover-dropdown.js"></script>
    <script defer src="${basePath}/default/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="${basePath}/default/assets/bxslider/jquery.bxslider.js"></script>

    <script src="${basePath}/default/js/jquery.easing.min.js"></script>
    <script src="${basePath}/default/js/link-hover.js"></script>
    
     <!--common script for all pages-->
    <script src="${basePath}/default/js/common-scripts.js"></script>


  <script>


  </script>

  </body>
</html>